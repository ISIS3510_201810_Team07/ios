//
//  Extensions.swift
//  EyeOnMe
//
//  Created by Paula Ramirez on 9/29/18.
//  Copyright © 2018 Paula Ramirez. All rights reserved.
//

import UIKit

extension UIColor {
    static func rgb (red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor{
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}


