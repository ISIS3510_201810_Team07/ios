//
//  ViewController.swift
//  EyeOnMe
//
//  Created by Paula Ramirez on 9/23/18.
//  Copyright © 2018 Paula Ramirez. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var userTxt: UITextField!
    @IBOutlet weak var passTxt: UITextField!
    @IBOutlet weak var plusBttn: UIButton!
    @IBOutlet weak var signUpBttn: UIButton!
    
    let plusPicButton: UIButton = {
        let button = UIButton(type: .system)
        //button.setImage(plus.withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handlePlus), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view.addSubview(plusPicButton)
        
        emailTxt.placeholder = "Email"
        emailTxt.backgroundColor = UIColor(white: 0, alpha: 0.03)
        emailTxt.borderStyle = .roundedRect
        emailTxt.font = UIFont.systemFont(ofSize: 14)
        emailTxt.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        
        userTxt.placeholder = "Username"
        userTxt.backgroundColor = UIColor(white: 0, alpha: 0.03)
        userTxt.borderStyle = .roundedRect
        userTxt.font = UIFont.systemFont(ofSize: 14)
        userTxt.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        
        passTxt.placeholder = "Password"
        passTxt.isSecureTextEntry = true
        passTxt.backgroundColor = UIColor(white: 0, alpha: 0.03)
        passTxt.borderStyle = .roundedRect
        passTxt.font = UIFont.systemFont(ofSize: 14)
        passTxt.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        
        signUpBttn.setTitle("Sign up!", for: .normal)
        signUpBttn.backgroundColor = UIColor.rgb(red: 86, green: 85, blue: 84)
        signUpBttn.layer.cornerRadius = 5
        signUpBttn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        signUpBttn.setTitleColor(.white, for: .normal)
        signUpBttn.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        signUpBttn.isEnabled = false
        
        plusPicButton.anchor(top: view.topAnchor, left: nil, bottom: nil, right: nil, paddingTop: 40, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 140, height: 140)
        
        plusPicButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
    }
    
    @objc func handleTextInputChange() {
        let isFormValid = emailTxt.text?.characters.count ?? 0 > 0 &&
                          emailTxt.text?.characters.count ?? 0 > 0 &&
                          emailTxt.text?.characters.count ?? 0 > 0
        
        if isFormValid {
            signUpBttn.backgroundColor = .blue
            signUpBttn.isEnabled = true
        } else {
            signUpBttn.isEnabled = false
            signUpBttn.backgroundColor = UIColor.rgb(red: 86, green: 85, blue: 84)
        }
    }
    
    @objc func handlePlus() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    @objc func handleSignUp() {
        //,email.characters.count >0
        guard let email = emailTxt.text else { return }
        guard let pass = passTxt.text else { return }
        guard let userNme = userTxt.text else { return }
        
//        validateFields(text: emailTxt.text ?? default value)
//        validateFields(text: passTxt.text ?? default value)
//        validateFields(text: userTxt.text ?? default value)
        
        Auth.auth().createUser(withEmail: email, password: pass, completion: { user, error in
            if let err = error {
                print("Fail", err)
            }
            print("Succesfull", Auth.auth().currentUser?.uid ?? 0)
            
            guard let uid = Auth.auth().currentUser?.uid else { return }
            
            let usernameVals = ["username":userNme]
            let values = [uid: usernameVals]
            Database.database().reference().child("users").updateChildValues(values, withCompletionBlock: { (err, ref) in
                if let err = err {
                    print("Failed to save user info to db", err)
                    return
                }
                print("Succcesfully saved user info in db")
            })
        })
        
    }
    
//    func validateFields(text: String) -> Bool{
//        if text.characters.count > 0 {
//            return true
//        }
//        return false
//    }
    
    fileprivate func setUpInputFields(){
        let stackView = UIStackView(arrangedSubviews: [emailTxt,userTxt,passTxt,signUpBttn])
        
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.spacing = 10
        
        view.addSubview(stackView)
        
        stackView.anchor(top: plusPicButton.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 20, paddingLeft: 40, paddingBottom: 0, paddingRight: 40, width: 0, height: 200)
    }
    
}

extension UIView {
    func anchor(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?, paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat, paddingRight: CGFloat, width: CGFloat, height: CGFloat){
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top{
            self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        if let left = left{
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        if let bottom = bottom{
            self.bottomAnchor.constraint(equalTo: bottom, constant: paddingBottom).isActive = true
        }
        if let right = right{
            self.rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
}
