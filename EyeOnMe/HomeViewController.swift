//
//  HomeViewController.swift
//  EyeOnMe
//
//  Created by Paula Ramirez on 9/30/18.
//  Copyright © 2018 Paula Ramirez. All rights reserved.
//

import UIKit
import Firebase

class HomeViewController: UIViewController {

    @IBOutlet weak var addContactButton: UIButton!
    @IBOutlet weak var addDestinationButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    
    
    override func viewDidLoad() {
        
        view.addSubview(addContactButton)
        view.addSubview(addDestinationButton)
        view.addSubview(startButton)
        
    }
    
}
